#!/usr/bin/env bash

# Translation
source /usr/bin/recbox-workflow-run_lang.sh

CONF_PATH="$HOME/.config/recbox-workflow-settings/conf"
AUTOSTART_PATH="$HOME/.config/autostart"
DAILY_PATH="$HOME/.config/recbox-workflow-settings/daily-desktop"
STUDIO_PATH="$HOME/.config/recbox-workflow-settings/studio-desktop"
ICON_PATH="/usr/share/icons/hicolor/scalable/apps/recbox-workflow-settings.svg"

daily_mode() {
	if [[ -d $HOME/.config/recbox-workflow-settings ]]; then
		echo -e ":: $SYSTEM_SETUP_START\n:: $DAILY_MODE\n==> $SESSION_AND_STARTUP"
		if [[ -z $(ls "$HOME/.config/autostart") ]]; then
			echo "  -> $NOTHING_TO_CONFIGURE"
		else
			if [[ $(find "$AUTOSTART_PATH" -type f -name '*.desktop' | wc -l) == $(grep -c "=" "$DAILY_PATH") ]]; then
				recbox-workflow-desktop.sh --daily
			elif [[  $(find "$AUTOSTART_PATH" -type f -name '*.desktop' | wc -l) != $(grep -c "=" "$DAILY_PATH") ]]; then
				zenity --error --title="$ERROR_DESKTOP_TITLE" --text="$ERROR_DESKTOP_TEXT" \
				--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
			fi
		fi
		mkdir -p /tmp/workflow; echo "USER=$USER" | tee /tmp/workflow/user > /dev/null
		echo "==> $SESSION_AND_STARTUP_COMPLETE"
		echo "==> $SYSTEMD_SERVICES"
		pkexec recbox-workflow-system.sh --daily
		if [[ -z $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') ]]; then
			echo "mode=daily" > "$CONF_PATH"
		else
			if [[ $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') == "studio" ]]; then
				sed -i '/mode/ s/studio/daily/' "$CONF_PATH"
			fi
		fi
		echo -e "\n:: $DAILY_MODE_COMPLETE\n==> $RESTART_REQUIRED\n"
		read -r -p ":: $ENTER_TO_EXIT "
	elif [[ ! -d $HOME/.config/recbox-workflow-settings ]]; then
		zenity --error --title="$ERROR_WIDGET_TITLE" --text="$ERROR_WIDGET_TEXT" --no-wrap
	fi
}

studio_mode() {
	if [[ -d $HOME/.config/recbox-workflow-settings ]]; then
		echo -e ":: $SYSTEM_SETUP_START\n:: $STUDIO_MODE\n==> $SESSION_AND_STARTUP"
		if [[ -z $(ls "$HOME/.config/autostart") ]]; then
			echo "  -> $NOTHING_TO_CONFIGURE"
		else

			if [[ $(find "$AUTOSTART_PATH" -type f -name '*.desktop' | wc -l) == $(grep -c "=" "$STUDIO_PATH") ]]; then
				recbox-workflow-desktop.sh --studio
			elif [[  $(find "$AUTOSTART_PATH" -type f -name '*.desktop' | wc -l) != $(grep -c "=" "$STUDIO_PATH") ]]; then
				zenity --error --title="$ERROR_DESKTOP_TITLE" --text="$ERROR_DESKTOP_TEXT" \
				--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
			fi
		fi
		mkdir -p /tmp/workflow; echo "USER=$USER" | tee /tmp/workflow/user > /dev/null
		echo "==> $SESSION_AND_STARTUP_COMPLETE"
		echo "==> $SYSTEMD_SERVICES"
		pkexec recbox-workflow-system.sh --studio
		if [[ -z $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') ]]; then
			echo "mode=studio" > "$CONF_PATH"
		else
			if [[ $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') == "daily" ]]; then
				sed -i '/mode/ s/daily/studio/' "$CONF_PATH"
			fi
		fi
		echo -e "\n:: $STUDIO_MODE_COMPLETE\n==> $RESTART_REQUIRED\n"
		read -r -p ":: $ENTER_TO_EXIT "
	elif [[ ! -d $HOME/.config/recbox-workflow-settings ]]; then
		zenity --error --title="$ERROR_WIDGET_TITLE" --text="$ERROR_WIDGET_TEXT" --no-wrap
	fi
}

case "$1" in
	--daily-mode | -d ) daily_mode;;
	--studio-mode | -s ) studio_mode;;
	*)
echo -e "
	Usage:\n
	recbox-workflow-run.sh [ OPTION ]\n
	Options:\n
	--daily-mode, -d
	--studio-mode, -s
" >&2
exit 1;;
esac
exit 0
