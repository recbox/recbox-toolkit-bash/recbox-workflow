## About:
**Workflow** is a utility to manage system settings for **Daily** and **Studio** usage, which means there is no need to dualboot or create a new user to have separated system configuration for daily usage and recording or mix and mastering sessions.

![workflow.gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-workflow.gif)

## Installation:
```
cd ~
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow.git
cd recbox-workflow
cp -r .config/autostart/* ~/.config/autostart
sudo cp -r etc/* /etc/
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity

Optionally for DAW:
pamac install jack2-dbus
```
## Translation:
Directories containing translations can be found here:
* [**usr/share/workflow/translations/run**](https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow/-/tree/main/usr/share/workflow/translations/run)
- [x] en_US
- [x] pl_PL
* [**usr/share/workflow/translations/settings**](https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow/-/tree/main/usr/share/workflow/translations/settings)
- [x] en_US
- [x] pl_PL
* [**/usr/share/workflow/translations/system**](https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow/-/tree/main/usr/share/workflow/translations/system)
- [x] en_US
- [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` files from:
* * usr/share/workflow/translations/run
* * usr/share/workflow/translations/settings
* * /usr/share/workflow/translations/system
* rename en_US with your system language
* replace text under quotation marks
```
FREQUENCY_SCALING_TLP_OPTION="Frequency scaling - TLP"
FREQUENCY_SCALING_TLP_DESC="Power Management for Laptops"
FREQUENCY_SCALING_CPU_POWER_OPTION="Frequency scaling - CPU Power"
FREQUENCY_SCALING_CPU_POWER_DESC="Power Management"
```
